/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "AIzaSyAmvHrrd_5AIyXIs8Esthxza5fQn7wwBoI",
    authDomain: "warm-boutique-423b8.firebaseapp.com",
    databaseURL: "https://warm-boutique-423b8.firebaseio.com",
    projectId: "warm-boutique-423b8",
    storageBucket: "warm-boutique-423b8.appspot.com",
    messagingSenderId: "145509008722",
    appId: "1:145509008722:web:1904df432d3bd12fac268b",
    measurementId: "G-RDJT5D65LT"
  }
};

import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { from, Observable, observable } from "rxjs";
import { Router, CanActivate } from "@angular/router";
import { staffType, StaffMem } from "../app/pages/staff/staff.service";
import {
  AngularFirestore,
  AngularFirestoreDocument
} from "@angular/fire/firestore";
import { switchMap } from "rxjs/operators";
import { of } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class LoginService {
  userData: Observable<firebase.User>;
  user$: Observable<StaffMem>;

  constructor(
    private angularFireAuth: AngularFireAuth,
    private router: Router,
    private afs: AngularFirestore
  ) {
    this.userData = angularFireAuth.authState;

    this.user$ = this.angularFireAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.afs.doc<StaffMem>(`staff/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      })
    );
  }
  isLoggedIn() {
    return this.angularFireAuth.auth.currentUser;
  }

  isAdmin() {
    return this.angularFireAuth.auth.currentUser.email == "admin@warm.com";
  }

  getMail() {
    return this.angularFireAuth.auth.currentUser.email;
  }

  SignIn(email: string, password: string) {
    this.angularFireAuth.auth
      .signInWithEmailAndPassword(email, password)
      .then(res => {
        console.log("You are successfully logged in");

        this.router.navigate(["/pages"]);
      })
      .catch(err => {
        console.log("Something is wrong", err.message);
      });
  }

  SignOut() {
    this.angularFireAuth.auth.signOut();
  }
  // getEmail() {
  //   return this.angularFireAuth.auth.currentUser.email;
  // }

  static isAdmin() {
    // console.log(LoginService.tempMail);
    // console.log(LoginService.tempMail == "dulajdanu@gmail.com");
    //return LoginService.tempMail == "dulajdanu@gmail.com";
    // console.log(LoginService.tempMail);
  }
}

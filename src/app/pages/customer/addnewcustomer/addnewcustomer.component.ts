import { Component, OnInit } from "@angular/core";
import { Customer, CustomerService } from "../customer.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { NbToastrService } from "@nebular/theme";
import { from, Observable } from "rxjs";

@Component({
  selector: "ngx-addnewcustomer",
  templateUrl: "./addnewcustomer.component.html",
  styleUrls: ["./addnewcustomer.component.scss"]
})
export class AddnewcustomerComponent implements OnInit {
  customers: Observable<Customer[]>;
  constructor(
    private cusService: CustomerService,
    private toastrService: NbToastrService
  ) {}

  ngOnInit() {}
  addNewCusForm = new FormGroup({
    first_name: new FormControl("", Validators.required),
    last_name: new FormControl("", Validators.required),
    email: new FormControl("", Validators.required),
    password: new FormControl("", Validators.required),
    contact_num: new FormControl("", Validators.required)
  });

  get first_name() {
    return this.addNewCusForm.get("first_name");
  }

  get last_name() {
    return this.addNewCusForm.get("last_name");
  }

  get email() {
    return this.addNewCusForm.get("email");
  }
  get password() {
    return this.addNewCusForm.get("password");
  }

  get contact_num() {
    return this.addNewCusForm.get("contact_num");
  }

  get customersList() {
    return (this.customers = this.cusService.getCustomers());
  }
  onSubmit(cus: Customer) {
    // TODO: Use EventEmitter with form value
    // console.warn(this.addProductForm.value);
    // console.log(this.addProductForm.value);
    console.log(cus);

    this.cusService.AddCusToDb(cus);
    this.addNewCusForm.reset();
    this.showToast("top-right", "success");
  }
  showToast(position, status) {
    this.toastrService.show("Customer Added Successfully", "Customer Added", {
      position,
      status
    });
  }
}

import { Component, OnInit } from "@angular/core";
import { SmartTableData } from "../../../@core/data/smart-table";
import { LocalDataSource } from "ng2-smart-table";
import { CustomerService } from '../customer.service';
@Component({
  selector: "ngx-viewcustomers",
  templateUrl: "./viewcustomers.component.html",
  styleUrls: ["./viewcustomers.component.scss"]
})
export class ViewcustomersComponent implements OnInit {
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>'
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>'
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true
    },
    columns: {
      // id: {
      //   title: "ID",
      //   type: "number"
      // },
      first_name: {
        title: "First Name",
        type: "string"
      },
      last_name: {
        title: "Last Name",
        type: "string"
      },
      contact_num: {
        title: "Username",
        type: "string"
      },
      email: {
        title: "E-mail",
        type: "string"
      },
      // age: {
      //   title: "Age",
      //   type: "number"
      // }
    }
  };

  source;

  constructor(private customerService: CustomerService) {
    customerService.getAll().subscribe(result=>{
      this.source=result;
    })
  }

  onDeleteConfirm(event): void {
    if (window.confirm("Are you sure you want to delete?")) {
      // event.confirm.resolve();
      // console.log(event.data.id);
      this.customerService.delete(event.data.id);
    } else {
      event.confirm.reject();
    }
  }

  onSaveConfirm(event): void {
    if (window.confirm("Are you sure you want to edit this?")) {
      // event.confirm.resolve();
      // event.newData["name"] += " + added in code";
      // console.log(event.data);
      // console.log(event.newData);

      this.customerService.edit(event.data.id, event.newData);
    } else {
      event.confirm.reject();
    }
  }

  ngOnInit() {}
}

// 4792210100194;
import { Component, OnInit } from "@angular/core";
import { Customer, CustomerService } from "../../customer/customer.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { NbToastrService } from "@nebular/theme";
import { from, Observable } from "rxjs";
import { Ng2SmartTableModule } from "ng2-smart-table";
import { CheckoutService } from "../checkout.service";
import { OrdersService } from "../orders.service";
import { DatePipe } from "@angular/common";
import {
  Product,
  Category,
  ProductService
} from "../../inventory/product.service";

@Component({
  selector: "ngx-checkoutpage",
  templateUrl: "./checkoutpage.component.html",
  styleUrls: ["./checkoutpage.component.scss"]
})
export class CheckoutpageComponent implements OnInit {
  public searchTerm: string;
  public places = [
    { name: "Afghanistan", code: "AF" },
    { name: "Afghanistan", code: "AF" },
    { name: "Åland Islands", code: "AX" },
    { name: "Albania", code: "AL" },
    { name: "Algeria", code: "DZ" }
    // Copy and paste from link above!
  ];
  finalDate: string;
  today: number = Date.now();
  userOption; //useroption

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>'
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true
    },
    columns: {
      // id: {
      //   title: "ID",
      //   type: "number"
      // },
      product_name: {
        title: "Item Name",
        type: "string"
      },
      quantity: {
        title: "Quantity",
        type: "number"
      },
      // discount: {
      //   title: "Discount",
      //   type: "number"
      // },
      totalPrice: {
        title: "Total",
        type: "number"
      }
    }
  };

  onDeleteConfirm(event): void {
    if (window.confirm("Are you sure you want to delete?")) {
      // event.confirm.resolve();
      // console.log(event.data.id);
      this.checkoutService.delete(event.data.id);
    } else {
      event.confirm.reject();
    }
  }

  source;
  public now: Date = new Date();
  // today: number = Date.now();

  constructor(
    private cusService: CustomerService,
    private toastrService: NbToastrService,
    private checkoutService: CheckoutService,
    private ordersService: OrdersService,
    private datePipe: DatePipe,
    private productService: ProductService
  ) {
    setInterval(() => {
      this.now = new Date();
    }, 1);

    this.finalDate = datePipe.transform(this.date, "yyyy.MM.dd");

    productService.getAll().subscribe(result => {
      this.source = result;
    });
  }

  productName = "Name";

  cart$;
  myCart: any[] = [];
  shoppingCartItemCount: number;
  totalPrice: number;

  orderProfit;
  discount = 0;
  discountP = 0;

  async ngOnInit() {
    let cart$ = await this.checkoutService.getCart();
    cart$.subscribe(cart => {
      this.shoppingCartItemCount = 0;
      this.totalPrice = 0;
      this.orderProfit = 0;
      for (let ticketId in cart) {
        this.shoppingCartItemCount += cart[ticketId]["quantity"];
        this.orderProfit += Number(
          (cart[ticketId]["product_price"] - cart[ticketId]["unit_price"]) *
            cart[ticketId]["quantity"]
        );
        console.log(this.orderProfit);
        this.totalPrice += Number(
          cart[ticketId]["product_price"] * cart[ticketId]["quantity"]
        );
        // this.shoppingCartItemCount += cart[ticketId]["quantity"];
        // this.totalPrice += Number(cart[ticketId]["ticket"]["price"]);
        this.myCart = cart;
      }
      console.log(this.myCart);
      console.log(this.totalPrice);
    });
  }

  addNewCusForm = new FormGroup({
    first_name: new FormControl("", Validators.required),
    last_name: new FormControl("", Validators.required),
    email: new FormControl("", Validators.required),
    password: new FormControl("abc", Validators.required),
    contact_num: new FormControl("", Validators.required)
  });

  addNewItemForm = new FormGroup({
    item_name: new FormControl("", [Validators.nullValidator]),
    item_quantity: new FormControl("", Validators.required),
    item_discount: new FormControl(""),
    item_code: new FormControl("abc", Validators.required)
  });
  get item_name() {
    return this.addNewItemForm.get("item_name");
  }
  get item_quantity() {
    return this.addNewItemForm.get("item_quantity");
  }
  get item_discount() {
    return this.addNewItemForm.get("item_discount");
  }
  get item_code() {
    return this.addNewItemForm.get("item_code");
  }

  onSubmitCustomer(cus: Customer) {
    // TODO: Use EventEmitter with form value
    // console.warn(this.addProductForm.value);
    // console.log(this.addProductForm.value);
    // console.log(cus);

    this.cusService.AddCusToDb(cus);
    this.addNewCusForm.reset();
    this.showToast("top-right", "success");
  }
  onSubmitItem(val: any) {
    // this.source.concat(val);
    console.log(val);
  }

  get first_name() {
    return this.addNewCusForm.get("first_name");
  }

  get last_name() {
    return this.addNewCusForm.get("last_name");
  }

  get email() {
    return this.addNewCusForm.get("email");
  }
  get password() {
    return this.addNewCusForm.get("password");
  }

  get contact_num() {
    return this.addNewCusForm.get("contact_num");
  }

  get time() {
    return this.now.getTime();
  }
  get date() {
    return this.now.getDate();
  }
  showToast(position, status) {
    this.toastrService.show("Customer Added Successfully", "Customer Added", {
      position,
      status
    });
  }

  customerPay: number = 0;
  map = new Map();

  scanItem() {
    let QR = (<HTMLInputElement>document.getElementById("code")).value;
    console.log(QR);
    if (QR != "") {
      this.checkoutService.scanItem(QR).subscribe(result => {
        // this.orders = result;
        // this.total = +this.orders.price;
        this.productName = result["product_name"];

        // this.myCart=result;
        this.checkoutService.addToCart(result);
      });
    }
    (<HTMLInputElement>document.getElementById("code")).value = "";
  }

  async checkout() {
    try {
      console.log(this.myCart);
      var localCart = this.myCart.map(item => {
        return { id: item.id, orderQuantity: item.quantity };
      });
      localCart.forEach(element => {
        let newQuantity;
        this.productService.getQuantity(element.id).subscribe(result => {
          newQuantity = result["product_quantity"] - element.orderQuantity;
          console.log(newQuantity);
          this.productService.redeemProduct(element.id, newQuantity);
        });
      });
      this.ordersService.addOrder(
        this.myCart,
        this.totalPrice,
        this.shoppingCartItemCount,
        this.orderProfit,
        this.discount
      );
    } catch (error) {
      alert(error.message);
    }

    // console.log(officersIds);
    this.myCart = [];
    // this.source=[];
    // localStorage.removeItem("cartId");
    // this.customerPay = 0;
    // this.totalPrice = 0;
    // this.discount = 0;
    // this.discountP = 0;
    // this.shoppingCartItemCount = 0;

    setTimeout(() => {
      localStorage.removeItem("cartId");

      window.location.reload();
    }, 6000);

    // window.location.reload();
    // this.ngOnInit();
  }

  loadItems() {
    console.log(this.source);
  }
  addItem(item: Product) {
    this.checkoutService.addToCart(item);
  }

  onSaveConfirm(event): void {
    if (window.confirm("Are you sure you want to edit this?")) {
      event.confirm.resolve();
      // event.newData["name"] += " + added in code";
      // console.log(event.data);
      console.log(event.newData);

      this.checkoutService.edit(event.data.id, event.newData);
    } else {
      event.confirm.reject();
    }
  }
}

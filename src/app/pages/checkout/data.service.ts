import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { from } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class DataService {
  private itemInList = new BehaviorSubject<any>("string message");
  currentMessage = this.itemInList.asObservable();
  constructor() {}

  changeMessage(message: string) {
    this.itemInList.next(message);
  }
}

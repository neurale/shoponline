import { NgModule } from "@angular/core";
import { CheckoutComponent } from "./checkout.component";
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
  NbToastrModule,
  NbAlertModule,
  NbListModule
} from "@nebular/theme";

import { ThemeModule } from "../../@theme/theme.module";

import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { Ng2SmartTableModule } from "ng2-smart-table";
import { CheckoutpageComponent } from "./checkoutpage/checkoutpage.component";
import { NgxPrintModule } from "ngx-print";
import { Ng2SearchPipeModule } from "ng2-search-filter";
import { MatFormFieldModule } from "@angular/material/form-field";

import { MatIconModule } from "@angular/material/icon";

import { MatListModule } from "@angular/material/list";

@NgModule({
  imports: [
    ThemeModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbRadioModule,
    NbDatepickerModule,
    NbSelectModule,
    NbIconModule,
    Ng2SmartTableModule,
    FormsModule,
    ReactiveFormsModule,
    NbToastrModule.forRoot(),
    NbAlertModule,
    NgxPrintModule,
    Ng2SearchPipeModule,
    MatFormFieldModule,
    MatIconModule,
    MatListModule,
    NbListModule
  ],
  declarations: [CheckoutComponent, CheckoutpageComponent]
})
export class CheckoutModule {}

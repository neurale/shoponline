import { Component, Input, OnDestroy } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators';

import { TrafficList } from '../../../../@core/data/traffic-list';
import { TrafficCardService } from '../traffic-card.service';
import { DashboardService } from '../../dashboard.service';

@Component({
  selector: 'ngx-traffic-front-card',
  styleUrls: ['./traffic-front-card.component.scss'],
  templateUrl: './traffic-front-card.component.html',
})
export class TrafficFrontCardComponent implements OnDestroy {

  private alive = true;

  frontCardData;
  // @Input() frontCardData: TrafficList;


  // testData = [{date:"Test",value:100,delta:{up:false,value:100}}];

  currentTheme: string;

  constructor(private themeService: NbThemeService,private dashboardService: DashboardService) {
    this.themeService.getJsTheme()
      .pipe(takeWhile(() => this.alive))
      .subscribe(theme => {
        this.currentTheme = theme.name;
    });
    this.dashboardService.getWeek().subscribe(result => {
      this.frontCardData = result;
      console.log(this.frontCardData)
    });

  }

  trackByDate(_, item) {
    return item.date;
  }

  ngOnDestroy() {
    this.alive = false;
  }
}

import { Component } from "@angular/core";
import { DashboardService } from "./dashboard.service";

@Component({
  selector: "ngx-ecommerce",
  templateUrl: "./e-commerce.component.html"
})
export class ECommerceComponent {
  todayTotalItems = 0;
  todayTotalProfit = 0;

  stockValue;
  stockCostValue;
  salesValue;
  constructor(private dashService: DashboardService) {
    this.dashService.getToday().subscribe(result => {
      this.todayTotalItems = result["itemCount"];
      this.todayTotalProfit = result["profit"];
    });
    this.dashService.getStock().subscribe(result => {
      this.stockValue = 0;
      this.stockCostValue = 0;

      result.forEach(item => {
        this.stockValue +=
          item["product_selling_price"] * item["product_quantity"];
        this.stockCostValue +=
          item["product_unit_price"] * item["product_quantity"];
      });
    });
    this.dashService.getOrder().subscribe(result=>{
      this.salesValue=0;
      result.forEach(item=>{
        this.salesValue += item["totalPrice"];
      })
    });
  }
}

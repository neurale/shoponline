import { Component, OnDestroy } from "@angular/core";
import { PieChart, EarningData } from "../../../../@core/data/earning";
import { takeWhile } from "rxjs/operators";
import { DatePipe } from "@angular/common";

@Component({
  selector: "ngx-earning-card-back",
  styleUrls: ["./earning-card-back.component.scss"],
  templateUrl: "./earning-card-back.component.html"
})
export class EarningCardBackComponent implements OnDestroy {
  myDate = new Date();
  finalDate: string;
  print() {
    this.finalDate = this.datePipe.transform(this.myDate, "yyyy.MM.dd"); //this is the user selected date
    console.log(this.finalDate);
  }
  private alive = true;

  earningPieChartData: PieChart[];
  name: string;
  color: string;
  value: number;
  defaultSelectedCurrency: string = "Bitcoin";

  constructor(private earningService: EarningData, private datePipe: DatePipe) {
    this.earningService
      .getEarningPieChartData()
      .pipe(takeWhile(() => this.alive))
      .subscribe(earningPieChartData => {
        this.earningPieChartData = earningPieChartData;
      });
  }

  changeChartInfo(pieData: { value: number; name: string; color: any }) {
    this.value = pieData.value;
    this.name = pieData.name;
    this.color = pieData.color;
  }

  ngOnDestroy() {
    this.alive = false;
  }
}

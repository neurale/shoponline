import { Component } from "@angular/core";
import { DatePipe } from "@angular/common";

@Component({
  selector: "ngx-earning-card",
  styleUrls: ["./earning-card.component.scss"],
  templateUrl: "./earning-card.component.html"
})
export class EarningCardComponent {
  myDate = new Date();
  constructor(private datePipe: DatePipe) {}
  finalDate: string;
  print() {
    this.finalDate = this.datePipe.transform(this.myDate, "yyyy.MM.dd"); //this is the user selected date
    console.log(this.finalDate);
  }
}

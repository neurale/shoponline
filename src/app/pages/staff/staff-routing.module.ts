import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { StaffComponent } from "./staff.component";
import { AddnewmemberComponent } from "./addnewmember/addnewmember.component";
import { ViewmembersComponent } from "./viewmembers/viewmembers.component";

const routes: Routes = [
  { path: "", component: StaffComponent },
  { path: "addnew", component: AddnewmemberComponent },
  { path: "viewmembers", component: ViewmembersComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StaffRoutingModule {}

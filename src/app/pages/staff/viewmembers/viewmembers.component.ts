import { Component, OnInit } from "@angular/core";
import { SmartTableData } from "../../../@core/data/smart-table";
import { LocalDataSource } from "ng2-smart-table";
import { StaffService } from "../staff.service";
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: "ngx-viewmembers",
  templateUrl: "./viewmembers.component.html",
  styleUrls: ["./viewmembers.component.scss"]
})
export class ViewmembersComponent implements OnInit {
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>'
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true
    },
    columns: {
      first_name: {
        title: "First Name",
        type: "string"
      },
      last_name: {
        title: "Last Name",
        type: "string"
      },
      contact_num: {
        title: "Username",
        type: "string"
      },
      email: {
        title: "E-mail",
        type: "string"
      }
    }
  };

  source;
  source2;

  constructor(private staffService: StaffService, private angularFire: AngularFireAuth) {
    staffService.getAll().subscribe(result => {
      this.source = result;
    });
    // staffService.Staffget().subscribe(res => {
    //   this.source2 = res;
    // });
  }

  onDeleteConfirm(event): void {
    if (window.confirm("Are you sure you want to delete?")) {
      // event.confirm.resolve();
      // console.log(event.data.id);
      this.staffService.delete(event.data.id);
      this.angularFire.auth.signInWithEmailAndPassword(event.data["email"], event.data["password"])
        .then((val) => {
          val.user.delete();
        });
    } else {
      event.confirm.reject();
    }
  }

  onSaveConfirm(event): void {
    if (window.confirm("Are you sure you want to edit this?")) {
      // event.confirm.resolve();
      // event.newData["name"] += " + added in code";
      // console.log(event.data);
      // console.log(event.newData);

      this.staffService.edit(event.data.id, event.newData);
    } else {
      event.confirm.reject();
    }
  }

  ngOnInit() { }
  test() {
    console.log(this.source2);
  }
}

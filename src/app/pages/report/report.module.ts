import { NgModule } from "@angular/core";
import { CommonModule, DatePipe } from "@angular/common";

import { ReportRoutingModule } from "./report-routing.module";
import { ReportComponent } from "./report.component";
import { MonthlyreportComponent } from "./monthlyreport/monthlyreport.component";
import { DailyreportComponent } from "./dailyreport/dailyreport.component";
import {
  NbCardModule,
  NbInputModule,
  NbSelectModule,
  NbButtonModule,
  NbCalendarModule,
  NbDatepickerModule
} from "@nebular/theme";
import { Ng2SmartTableModule } from "ng2-smart-table";
import { NbDateFnsDateModule } from "@nebular/date-fns";
import { from } from "rxjs";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [ReportComponent, MonthlyreportComponent, DailyreportComponent,],
  imports: [
    CommonModule,
    ReportRoutingModule,
    NbCardModule,
    NbInputModule,
    NbSelectModule,
    NbButtonModule,
    Ng2SmartTableModule,
    NbCalendarModule,
    NbDatepickerModule,
    NbDateFnsDateModule,
    ReactiveFormsModule
  ],
  providers: [DatePipe]
})
export class ReportModule {}

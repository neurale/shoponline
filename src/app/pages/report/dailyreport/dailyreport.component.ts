import { Component, OnInit } from "@angular/core";
import { SmartTableData } from "../../../@core/data/smart-table";
import { LocalDataSource } from "ng2-smart-table";
import { ReportService } from "../report.service";
import { FormControl } from "@angular/forms";
import { DatePipe } from "@angular/common";
@Component({
  selector: "ngx-dailyreport",
  templateUrl: "./dailyreport.component.html",
  styleUrls: ["./dailyreport.component.scss"]
})
export class DailyreportComponent implements OnInit {
  dateSelected = new FormControl("");
  settings = {
    actions: false,
    columns: {
      // id: {
      //   title: "Order ID",
      //   type: "number"
      // },
      product_name: {
        title: "Item",
        type: "string"
      },
      quantity: {
        title: "Quantity",
        type: "string"
      },
      product_price: {
        title: "Total Price",
        type: "string"
      }
      // totalPrice: {
      //   title: "Total Price",
      //   type: "string"
      // }
      // itemCount: {
      //   title: "Item Count",
      //   type: "string"
      // },
      // orderProfit: {
      //   title: "Profit",
      //   type: "number"
      // }
    }
  };

  source;
  bills;
  tempArray: any[] = [];
  // source: LocalDataSource = new LocalDataSource();

  constructor(
    private service: SmartTableData,
    private reportService: ReportService,
    private datePipe: DatePipe
  ) {
    // const data = this.service.getData();
    // this.source.load(data);
  }
  handleDateChange(event) {
    //  console.log(this.getDate(event));

    this.reportService.getSales(this.getDate(event)).subscribe(sales => {
      this.source = sales;
      console.log(this.source);
    });
  }
  finalDate: string;
  today: number = Date.now();
  getAll() {
    this.source = null;
    this.finalDate = this.datePipe.transform(
      this.dateSelected.value,
      "yyyy.MM.dd"
    ); //this is the user selected date
    console.log(this.finalDate);

    this.reportService.getSales(this.finalDate).subscribe(sales => {
      // sales['order'];i
      this.bills = sales;

      for (let index in sales) {
        sales[index]["order"].forEach(data => {
          this.tempArray.push({
            id: sales[index]["id"],
            product_name: data["product_name"],
            quantity: data["quantity"],
            product_price: data["totalPrice"]
          });
        });

        // for (let order in sales[index]["order"])
        //   console.log(sales[index]["order"][order]["product_name"]);
        // console.log(sales[index]["order"][order]["product_price"]);
        // console.log(sales[index]["order"][order]["quantity"]);
        // console.log(sales[index]["order"]["product_price"]);
      }
      // this.source = this.tempArray;
      // console.log(this.source);
    });
  }
  billArray: any[] = [];
  viewBillDetails(id) {
    this.billArray = [];
    this.tempArray.forEach(element => {
      if (element["id"] == id) {
        this.billArray.push(element);
        // console.log(element['id'])
      }
    });
    this.source = this.billArray;
  }

  private getDate(d) {
    const date = new Date(d);
    const month = ("0" + (date.getMonth() + 1)).slice(-2);
    const day = ("0" + date.getDate()).slice(-2);
    const year = date.getFullYear().toString();

    return day + month + year;
  }
  onDeleteConfirm(event): void {
    if (window.confirm("Are you sure you want to delete?")) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  showDate(value) {
    console.log(value);
  }

  ngOnInit() {}
}

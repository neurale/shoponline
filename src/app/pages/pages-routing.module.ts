import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";

import { PagesComponent } from "./pages.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { ECommerceComponent } from "./e-commerce/e-commerce.component";
import { NotFoundComponent } from "./miscellaneous/not-found/not-found.component";
import { CheckoutComponent } from "./checkout/checkout.component";
import { LoginComponent } from "../login/login.component";
import { canActivate } from "@angular/fire/auth-guard";
import { AdminAuthService } from "../admin-auth.service";

const routes: Routes = [
  {
    path: "",
    component: PagesComponent,
    children: [
      {
        path: "dashboard",
        component: ECommerceComponent
      },

      {
        path: "checkout",
        component: CheckoutComponent
      },
      {
        path: "inventory",
        loadChildren: () =>
          import("./inventory/inventory.module").then(m => m.InventoryModule)
      },

      {
        path: "staffM",
        canActivate: [AdminAuthService],
        loadChildren: () =>
          import("./staff/staff.module").then(m => m.StaffModule)
      },
      {
        path: "reportM",
        canActivate: [AdminAuthService],
        loadChildren: () =>
          import("./report/report.module").then(m => m.ReportModule)
      },
      {
        path: "customers",
        loadChildren: () =>
          import("./customer/customer.module").then(m => m.CustomerModule)
      },
      {
        path: "",
        redirectTo: "dashboard",
        pathMatch: "full"
      },
      {
        path: "**",
        component: NotFoundComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule {}

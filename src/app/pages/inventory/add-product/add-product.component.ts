import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { Validators } from "@angular/forms";
import { ProductService, Product } from "../product.service";
import { from, Observable } from "rxjs";
import { Category } from "../product.service";
import { NbToastrService } from "@nebular/theme";

@Component({
  selector: "ngx-add-product",
  templateUrl: "./add-product.component.html",
  styleUrls: ["./add-product.component.scss"]
})
export class AddProductComponent implements OnInit {
  status = "false";
  categories: Observable<Category[]>;

  constructor(
    private productService: ProductService,
    private toastrService: NbToastrService
  ) {}

  ngOnInit() {}

  addProductForm = new FormGroup({
    id: new FormControl(""),
    product_name: new FormControl("", Validators.required),
    product_des: new FormControl(""),
    product_selling_price: new FormControl("", Validators.required),
    product_unit_price: new FormControl("", Validators.required),
    product_quantity: new FormControl("", Validators.required),
    product_category: new FormControl("", Validators.required)
  });

  addNewCategory = new FormGroup({
    // category_id: new FormControl("123"),
    category_name: new FormControl("", Validators.required)
  });

  get id() {
    return this.addProductForm.get("id");
  }

  get product_name() {
    return this.addProductForm.get("product_name");
  }

  get product_selling_price() {
    return this.addProductForm.get("product_selling_price");
  }
  get product_unit_price() {
    return this.addProductForm.get("product_unit_price");
  }

  get product_quantity() {
    return this.addProductForm.get("product_quantity");
  }

  get product_category() {
    return this.addProductForm.get("product_category");
  }

  get categoryName() {
    return this.addNewCategory.get("category_name");
  }

  onSubmit(pro: Product) {
    // TODO: Use EventEmitter with form value
    // console.warn(this.addProductForm.value);
    // console.log(this.addProductForm.value);
    this.productService.AddProductToDb(pro);
    console.log(pro);
    this.addProductForm.reset();
    this.showToast("top-right", "success");
  }

  get Categories() {
    return (this.categories = this.productService.Categoriesget());
  }
  onSubmitCategory(cat: Category) {
    this.productService.AddCategoryToDb(cat);
  }

  showToast(position, status) {
    this.toastrService.show("Item Added Successfully", "Item Added", {
      position,
      status
    });
  }
}

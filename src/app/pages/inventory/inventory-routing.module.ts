import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { InventoryComponent } from "./inventory.component";
import { AddProductComponent } from "./add-product/add-product.component";
import { ViewProductComponent } from "./view-product/view-product.component";
import { BarcodeComponent } from "./barcode/barcode.component";
import { AdminAuthService } from "../../admin-auth.service";

const routes: Routes = [
  {
    path: "",
    component: InventoryComponent,
    children: [
      {
        path: "add-product",
        component: AddProductComponent
      },
      {
        path: "view-product",
        component: ViewProductComponent,
        canActivate: [AdminAuthService]
      },
      {
        path: "barcode",
        component: BarcodeComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InventoryRoutingModule {}

export const routedComponents = [
  InventoryComponent,
  AddProductComponent,
  ViewProductComponent
];

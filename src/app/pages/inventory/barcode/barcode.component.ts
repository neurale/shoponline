import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import {
  Product,
  Category,
  ProductService
} from "../../inventory/product.service";

@Component({
  selector: "ngx-barcode",
  templateUrl: "./barcode.component.html",
  styleUrls: ["./barcode.component.scss"]
})
export class BarcodeComponent implements OnInit {
  source;
  public searchTerm: string;

  constructor(private productService: ProductService) {
    productService.getAll().subscribe(result => {
      this.source = result;
    });
    console.log(this.source);
  }

  // generateBarCode = new FormGroup({
  //   barcode_number: new FormControl("", Validators.required)
  // });

  ngOnInit() {}

  barcodeVal: string;
  showBarcode: boolean = false;

  generateBarcode(val: string) {
    this.barcodeVal = val;
    this.showBarcode = true;
  }

  addItem(item: Product) {
    console.log(item.product_name);
    this.generateBarcode(item.id);
  }
}

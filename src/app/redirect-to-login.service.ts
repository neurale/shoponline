import { Injectable } from "@angular/core";
import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from "@angular/router";
import { LoginService } from "../app/login.service";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class RedirectToLoginService implements CanActivate {
  constructor(private authService: LoginService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (this.authService.isLoggedIn) {
      return this.router.navigate(["pages"]);
    } else {
      // return this.router.navigate(["login"]);
    }
  }
}

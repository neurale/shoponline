import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { LoginService } from "../login.service";
import { from } from 'rxjs';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private loginService: LoginService) { }

  ngOnInit() {
  }
  loginForm = new FormGroup(
    {
      e_mail: new FormControl("", Validators.required),
      password: new FormControl("", Validators.required)
    },

  );
  get e_mail() {
    return this.loginForm.get("e_mail");
  }
  get password() {
    return this.loginForm.get("password");
  }

  onSubmit(e_mail, password) {
    this.loginService.SignIn(e_mail, password);
    console.log("hello")
  }

}

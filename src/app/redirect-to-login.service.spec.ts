import { TestBed } from '@angular/core/testing';

import { RedirectToLoginService } from './redirect-to-login.service';

describe('RedirectToLoginService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RedirectToLoginService = TestBed.get(RedirectToLoginService);
    expect(service).toBeTruthy();
  });
});
